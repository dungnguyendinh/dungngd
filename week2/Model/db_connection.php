<?php
class db_connection{

    private $serverName = "localhost";
    private $username = "root";
    private $password = "";
    private $myDB = "bss";

    public function connect(){
        try{
            $conn = new PDO("mysql:host=$this->serverName;dbname=$this->myDB",$this->username,$this->password);
            $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
        }catch(PDOException $e){
            echo "Connection failed" . $e->getMessage();
        }
        return $conn;
    }

}
