<?php

namespace Bss\HelloWorld\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Info extends Template
{
    protected $pageFactory;

    public function __construct(Template\Context $context, RequestInterface $request)
    {
        parent::__construct($context);
        $this->request = $request;
    }

    public function getInfo()
    {
        $name = $this->request->getParam('name', 'Dunxg');
        $dob = $this->request->getParam('dob', '12/08/1999');
        $address = $this->request->getParam('address', 'HN');
        $description = $this->request->getParam('description', 'abcxyz');
        $arr = array($name, $dob, $address, $description);
        return $arr;
    }
}
