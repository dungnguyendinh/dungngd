<?php

namespace Bss\HelloWorld\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Bss\HelloWorld\Helper\Data;

class Index extends Template
{
    protected Data $helperData;

    public function __construct(Template\Context $context, RequestInterface $request, Data $helperData)
    {
        parent::__construct($context);
        $this->request = $request;
        $this->helperData = $helperData;
    }

    public function getHello()
    {
        return 'Hello World';
    }

    public function getEnable()
    {
        return $this->helperData->getGeneralConfig('enable');
    }

    public function getTitle()
    {
        return $this->helperData->getGeneralConfig('page_title');
    }

    public function getDes()
    {
        return $this->helperData->getGeneralConfig('page_description');
    }
}
