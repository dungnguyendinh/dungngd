<?php
declare(strict_types=1);

namespace Bss\HelloWorld\Controller\Index;

use Magento\Framework\App\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;

class InfoJSON extends Action\Action
{
    protected $_resultJsonFactory;

    /**
     * RouterJson constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param RequestInterface $request
     */
    public function __construct(
        Context     $context,
        JsonFactory $resultJsonFactory
    )
    {
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Return information
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $params = [
            "Name" => "Dungx",
            "DOB" => "August 12th 1999",
            "Address" => "HN",
            "Description" => "abc",
        ];
        return $result->setData($params);
    }
}
