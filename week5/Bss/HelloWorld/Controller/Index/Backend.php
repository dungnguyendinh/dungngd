<?php

namespace Bss\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Bss\HelloWorld\Helper\Data;
use Magento\Framework\Controller\Result\ForwardFactory;

class Backend extends Action
{
    protected PageFactory $pageFactory;
    protected Data $helperData;
    protected ForwardFactory $forwardFactor;

    public function __construct(Context $context, PageFactory $pageFactory, Data $helperData, ForwardFactory $forwardFactory)
    {
        $this->helperData = $helperData;
        $this->pageFactory = $pageFactory;
        $this->forwardFactory = $forwardFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $enable = $this->helperData->getGeneralConfig('enable');
        if ($enable == 0) {
            $resultForward = $this->forwardFactory->create();
            $resultForward->setController('index');
            $resultForward->forward('defaultNoRoute');
            return $resultForward;
        } else {
            return $this->pageFactory->create();
        }
    }
}
