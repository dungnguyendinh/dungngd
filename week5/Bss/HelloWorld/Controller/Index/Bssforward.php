<?php

namespace Bss\HelloWorld\Controller\Index;

use Magento\Framework\App\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;

class Bssforward extends Action\Action
{
    /**
     * @var ForwardFactory
     */
    protected $_resultForwardFactory;

    /**
     * Page4 constructor.
     * @param Context $context
     * @param ForwardFactory $_resultForwardFactory
     */
    public function __construct(
        Context        $context,
        ForwardFactory $_resultForwardFactory
    )
    {
        $this->_resultForwardFactory = $_resultForwardFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultForward = $this->_resultForwardFactory->create();
        $resultForward->setController('index')
            ->setModule('cms')
            ->forward('index');
        return $resultForward;
    }
}
