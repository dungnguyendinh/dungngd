define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Bss_Fresher/ko'
            },
            initialize: function () {
                this.Bss = ko.observableArray([]);
                this.idFresher = ko.observable('');
                this.nameFresher = ko.observable('');
                this.classFresher = ko.observable('');
                this.errorMessage = ko.observable('');
                self = this;
                this._super();
            },

            addFresher: function () {
                if (this.idFresher() == '') {
                    this.errorMessage('Missing ID !');
                } else if (this.nameFresher() == '') {
                    this.errorMessage('Missing name !');
                } else if (this.classFresher() == '') {
                    this.errorMessage('Missing class');
                } else {
                    this.Bss.push({id: this.idFresher(), name: this.nameFresher(), classFr: this.classFresher()});
                    this.nameFresher('');
                    this.idFresher('');
                    this.classFresher('');
                }
            },
            deleteFresher: function () {
                self.Bss.remove(this);
            },
            removeAll: function () {
                this.Bss.removeAll();
            }
        });
    }
);
