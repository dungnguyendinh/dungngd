define([
    'jquery',
    'jquery/ui'
], function ($) {
    'use strict';
    $.widget('widget.fresher', {
        _create: function () {
            this._validateFresher();
        },
        _validateFresher: function () {
            let self = this;
            $("#submit").click(function () {
                var name = document.getElementById("name").value;
                var telephone = document.getElementById("telephone").value;
                var dob = document.getElementById("dob").value;
                var message = document.getElementById("message").value;
                if (name == "") {
                    $(".validate").empty();
                    $(".validate").append("<p>Missing name !</p>");
                } else if (telephone == "") {
                    $(".validate").empty();
                    $(".validate").append("<p>Missing telephone !</p>");
                } else if (dob == "") {
                    $(".validate").empty();
                    $(".validate").append("<p>Missing day of birth !</p>");
                } else {
                    $(".validate").empty();
                    self._display(name, telephone, dob, message);
                }
            })
        },
        _display: function (name, telephone, dob, message) {
            document.getElementById("popupName").innerHTML = "Name: " + name;
            document.getElementById("popupPhone").innerHTML = "Phone: " + telephone;
            document.getElementById("popupDob").innerHTML = "Day of birth: " + dob;
            if (message != "") {
                document.getElementById("popupMessage").innerHTML = "Message: " + message;
            } else {
                $("#popupMessage").empty();
            }
            require([
                "jquery",
                "Magento_Ui/js/modal/modal"
            ], function ($, modal) {
                var options = {
                    type: 'popup',
                    responsive: true,
                    title: 'Welcome BSS Fresher',
                    buttons: '',
                };
                var popup = modal(options, $('#welcome-popup'));
                $('#welcome-popup').modal('openModal');
            });
        }
    });
    return $.widget.fresher;
})
;
