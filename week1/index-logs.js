var data_logs = [
  { ID: '101', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '102', name: 'Refrigerator', action: 'Turn On', date: '2021-05-31' },
  { ID: '103', name: 'Fan', action: 'Turn Off', date: '2021-05-31' },
  { ID: '104', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '105', name: 'Washer', action: 'Turn Off', date: '2021-05-31' },
  { ID: '106', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '107', name: 'Refrigerator', action: 'Turn On', date: '2021-05-31' },
  { ID: '108', name: 'Fan', action: 'Turn Off', date: '2021-05-31' },
  { ID: '109', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '110', name: 'Washer', action: 'Turn Off', date: '2021-05-31' },
  { ID: '111', name: 'Fan', action: 'Turn Off', date: '2021-05-31' },
  { ID: '112', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '113', name: 'Washer', action: 'Turn Off', date: '2021-05-31' },
  { ID: '114', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '115', name: 'Refrigerator', action: 'Turn On', date: '2021-05-31' },
  { ID: '116', name: 'Fan', action: 'Turn Off', date: '2021-05-31' },
  { ID: '117', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '118', name: 'Washer', action: 'Turn Off', date: '2021-05-31' },
  { ID: '119', name: 'Fan', action: 'Turn Off', date: '2021-05-31' },
  { ID: '120', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '121', name: 'Washer', action: 'Turn Off', date: '2021-05-31' },
  { ID: '122', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '123', name: 'Refrigerator', action: 'Turn On', date: '2021-05-31' },
  { ID: '124', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '125', name: 'Washer', action: 'Turn Off', date: '2021-05-31' },
  { ID: '126', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '127', name: 'Refrigerator', action: 'Turn On', date: '2021-05-31' },
  { ID: '128', name: 'Fan', action: 'Turn Off', date: '2021-05-31' },
  { ID: '129', name: 'TV', action: 'Turn On', date: '2021-05-31' },
  { ID: '130', name: 'Washer', action: 'Turn Off', date: '2021-05-31' },
  { ID: '131', name: 'Fan', action: 'Turn Off', date: '2021-05-31' },
  { ID: '132', name: 'TV', action: 'Turn On', date: '2021-05-31' },
];

$(document).ready(function () {

  function reload(arr) {
    var table = "<thead><tr><th>Device ID</th><th>Name</th><th>Action</th><th>Date</th></tr></thead><tbody>";
    for (let i = 1; i <= arr.length; i++) {
      table += "<tr><td>" + i + "</td><td>" + arr[i - 1].name + "</td><td>" + arr[i - 1].action +"</td><td>" +arr[i - 1].date + "</td></tr>";
    };
    table += "</tbody>";
    $("table").html(table);
  };

  var arr = data_logs;
  reload(arr);

  function pagination(arr) {
    $('#container-pagination').empty();
    var rowsShow = 10;
    var rowsTotal = arr.length;
    var numPages = rowsTotal / rowsShow;
    for (i = 0; i < numPages; i++) {
      var pageNum = i + 1;
      $('#container-pagination').append('<a href="#" rel="' + i + '">' + '<div>' + pageNum + '</div></a>');
    }
    $('#table thead tr').show();
    $('#table tbody tr').hide();
    $('#table tbody tr').slice(0, rowsShow).show();
    $('#container-pagination a:first').addClass('active');
    $('#container-pagination a').bind('click', function () {
      $('#container-pagination a').removeClass('active');
      $(this).addClass('active');
      var currPage = $(this).attr('rel');
      var startItem = currPage * rowsShow;
      var endItem = startItem + rowsShow;
      $('#table tbody tr').css('opacity', '0.0').hide().slice(startItem, endItem).css('display', 'table-row').animate({ opacity: 1 }, 300);
    });
  }
  pagination(arr);

  $("#search").click(function () {
    x = [];
    var name = $("#input-search").val().toUpperCase();
    if (name == "") alert("Invalid input");
    else {
      for (let i = 0; i < data_logs.length; i++) {
        if (data_logs[i].name.toUpperCase() == name) {
          x.push(data_logs[i]);
          console.log(data_logs[i].id);
        }
      }
      reload(x);
      page(x);
    }
  });

});