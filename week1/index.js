//login.js
function validateForm(){
  let username = document.forms["loginForm"]["username"].value;
  let password = document.forms["loginForm"]["password"].value;
  if(username == ""){
    alert("Name must be filled out");
    return false;
  }
  if(password == ""){
    alert("Password must be filled out");
    return false;
  }
  if(username != "john" || password != "1234"){
    alert("Invalid input!");
    return false;
  }
}


//dashboar.js
var device = [
	{name:'TV', MAC:'00:1B:44:11:3A:B7', IP:'127.0.0.2', date:'2021-05-31', consumption: 50},
	{name:'Washer', MAC:'00:1B:44:11:3A:B8', IP:'127.0.0.3', date:'2021-05-31', consumption: 60},
	{name:'Refrigerator', MAC:'00:1B:44:11:3A:B9', IP:'127.0.0.4', date:'2021-05-31', consumption: 80},
	{name:'Selling Fan', MAC:'00:1B:44:11:3A:B2', IP:'127.0.0.5', date:'2021-05-31', consumption: 100},
];
print(device);
function addDevice(){
  var name = document.getElementById("device-name").value;
  var MAC = document.getElementById("device-MAC").value;
  var IP = document.getElementById("device-IP").value;
  var consumption = parseInt(document.getElementById("device-consumption").value);
  if(name == "" || MAC =="" || IP =="" || consumption =="") 
    alert("Missing Information!");
  else{
    alert("Insert success!");
    var b = {name:name, MAC : MAC, IP : IP, date:'2021-05-31', consumption : consumption};
    device.push(b);
    print(device)
  }
}

function print(device){
  var device_data='';
  var total = 0;
  device_data += '<thead><tr><th style="text-align: left; padding-left: 3%">Devices</th><th>MAC Address</th><th>IP</th><th>Create Date</th><th style="text-align: right; padding-right: 3%">Power Consumption(Kw/H)</th></tr></thead>';
  device_data +='<tbody>';
  for (var i = 0; i < device.length; ++i) {
    device_data +='<tr>';
    device_data +='<td style="text-align: left; padding-left: 3%">'+device[i]['name']+'</td>';
    device_data +='<td>'+device[i]['MAC']+'</td>';
    device_data +='<td>'+device[i]['IP']+'</td>';
    device_data +='<td>'+device[i]['date']+'</td>';
    device_data +='<td style="text-align: right; padding-right: 3%">'+device[i]['consumption']+'</td>';
    device_data +='<tr>';
    total += device[i]['consumption'];
  }
  device_data += '</tbody>';
  device_data += '<tfoot>'
  device_data +='<tr style="background: #0d01013d">';
  device_data +='<td style="text-align: left; padding-left: 3%">Total</td>';
  device_data +='<td></td>';
  device_data +='<td></td>';
  device_data +='<td></td>';
  device_data +='<td style="text-align: right; padding-right: 3%;">'+total+'</td>';
  device_data +='<tr>';
  device_data += '</tfoot>';
  document.getElementById("data-table").innerHTML = device_data;
};

//responsive
function menu(){
  var hidden=document.getElementById("menu");
  hidden.style.display='block';
}
$(document).ready(function () {
  $(window).resize(function() {
      var width = $(window).width();
      if (width < 600){
          $(document).click(function() {
              //Hide the menus if visible
              $('#menu').hide();
          });
          $('#iconMenu').click(function(event) {
              //Hide the menus if visible
              event.stopPropagation();
          });
      }
  });

});



//chart
const chart_name_data = device.map(device_data => device_data.consumption);
const chart_consumption_data = device.map(device_data => device_data.name);

const ctx = $('#myChart');
const myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: chart_consumption_data,
        datasets: [{
            label: 'Consumption Chart',
            data: chart_name_data,
            backgroundColor: [
                'rgba(255, 0, 0, 1)',
                'rgba(255, 255, 0, 1)',
                'rgba(0, 0, 255, 1)',
                'rgba(255, 64, 182, 1)'
            ],
        }]
    },
});
/*
const action = [
{
  name: 'Add Data',
  handler(chart) {
    const data = chart.data;
    if (data.datasets.length > 0) {
      data.labels.push('data #' + (data.labels.length + 1));

      for (let index = 0; index < data.datasets.length; ++index) {
        data.datasets[index].data.push(Utils.rand(0, 100));
      }

      chart.update();
    }
  }
]
}
*/
